package org.linkedopenactors.code.loaapp.controller;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Binding {

	private String name;
	private String value;
}
