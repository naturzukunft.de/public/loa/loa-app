package org.linkedopenactors.code.loaapp.controller;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.Repository;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;
import org.linkedopenactors.code.loaapp.controller.infrastructure.config.RDF4JRepositoryManager;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import de.naturzukunft.rdf4j.loarepository.PublicationLoa;
import de.naturzukunft.rdf4j.loarepository.PublicationRepo;
import de.naturzukunft.rdf4j.ommapper.ModelCreator;
import lombok.extern.slf4j.Slf4j;

@Controller
@Slf4j
public class LoaController {
	
	private RDF4JRepositoryManager rdf4jRepositoryManager;

	public LoaController(RDF4JRepositoryManager rdf4jRepositoryManager) {
		this.rdf4jRepositoryManager = rdf4jRepositoryManager;
	}

	
	@GetMapping(path = "/kvm/{publication}", produces = { "text/turtle" })
	public ResponseEntity<String> getPublicationTurtle(@PathVariable String publication, Model model) {
		PublicationLoa pub = getPublication(publication);
		String res = toRdf(RDFFormat.TURTLE, new ModelCreator<PublicationLoa>(pub).toModel());
		return new ResponseEntity<String>(res, HttpStatus.OK);
	}
	
	@GetMapping(path = "/kvm/{publication}", produces = { "application/json+ld" })
	public ResponseEntity<String> getPublicationJsonLd(@PathVariable String publication, Model model) {
		String res = toJsonLd(			
				new ModelCreator<PublicationLoa>(getPublication(publication))
				.toModel());
		return new ResponseEntity<String>(res, HttpStatus.OK);
	}
	
	@GetMapping(path = "/kvm/{publication}", produces = { "text/html" })
	public String getPublicationHtml(@PathVariable String publication, Model model) {
		
		PublicationLoa pub = getPublication(publication);
//		log.info("extracting the model");
//		org.eclipse.rdf4j.model.Model rdf4jModel = new ModelCreator<PublicationLoa>(pub).toModel();
		
		model.addAttribute("publication", pub);
		model.addAttribute("name", pub.getIdentifier());
		
//		String jsonLd = toJsonLd(rdf4jModel); // TODO maybe it works after the jsonld parser fix
//		model.addAttribute("jsonld", "{"+jsonLd+"}");
		model.addAttribute("pageTitle", "LinkedOpenActors - Publication - " + pub.getAbout().getName());
		
		return "publication";
	}


	private PublicationLoa getPublication(String publication) {
		PublicationRepo publicationRepo = rdf4jRepositoryManager.getPublicationRepo();
		log.info("reading publication: " + publication);
		List<PublicationLoa> publications = publicationRepo.getByIdentifier(publication);
		
		// TODO search the newest
		
//		String identifier = publications.stream().findFirst().map(it->it.getAbout().getName()).orElse("notFound");
		log.info("extracting the first publication from : " + publications.size());
		PublicationLoa pub = publications.stream().findFirst().orElseThrow(()->new RuntimeException("not found"));
		return pub;
	}

	@GetMapping("/publications")
	public String getAll(Model model) {
		Repository repository = rdf4jRepositoryManager.getKvmRepo();
		List<Binding> bindings = new ArrayList<>();
		try(RepositoryConnection con = repository.getConnection()) {
			TupleQuery tupleQuery = con.prepareTupleQuery(query());
			   try (TupleQueryResult result = tupleQuery.evaluate()) {//				   
			      while (result.hasNext()) {  // iterate over the result
			         BindingSet bindingSet = result.next();
			         bindingSet.getBindingNames().forEach(name->{
			        	 bindings.add(new Binding(name, bindingSet.getValue(name).stringValue()));			        	 
			         });
			         
			      }
			      model.addAttribute("bindings", bindings);
			   }
		}
		model.addAttribute("bindings", bindings);
		return "publications";
	}
	
	private String query() {
		return "PREFIX schema: <http://schema.org/> \n"
				+ "PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> \n"
				+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n"
				+ "SELECT DISTINCT *\n"
				+ "WHERE { \n"
				+ "	?s rdf:type schema:CreativeWork .  	\n"
				+ "  	?s schema:creativeWorkStatus ?status .\n"
				+ "	?s schema:license ?license .\n"
				+ "    ?s schema:version ?version .\n"
				+ "  	?s schema:identifier ?identifier .\n"
				+ "  	?s schema:description ?description .\n"
				+ "} LIMIT 1000";
	}
	
	private String toJsonLd(org.eclipse.rdf4j.model.Model model) {
		return toRdf(RDFFormat.JSONLD, model);
	}
	
	private String toRdf(RDFFormat rdfFormat, org.eclipse.rdf4j.model.Model model) {
		StringWriter sw = new StringWriter();
		Rio.write(model, sw, rdfFormat);		
		return sw.toString();
	}
	
}
